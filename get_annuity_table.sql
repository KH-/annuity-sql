CREATE OR REPLACE FUNCTION get_annuity_table("loan amount" NUMERIC, "period in months" NUMERIC, "year interest rate" NUMERIC) 
RETURNS TABLE ( 
     "nr"                  INT, 
     "payment_month"       TEXT, 
     "monthly payment"     NUMERIC, 
     "principal payment"   NUMERIC, 
     "interest"            NUMERIC, 
     "principal remaining" NUMERIC 
  )
  
AS $$
/* Declare vars used in calculations*/
DECLARE
  monthly_payment NUMERIC;
  row_remaining_payment NUMERIC;
  row_interest NUMERIC;
  row_principal_payment NUMERIC;
  
BEGIN

/*Calculate monthly payment which has to be done every payment period*/
monthly_payment := (("year interest rate"/100/12)*"loan amount")/(1 - pow((1 + ("year interest rate" / 100 / 12)), -"period in months"));

row_remaining_payment := "loan amount";

/*Create temp table for the data*/
CREATE TEMP TABLE  annuity ( 
     "nr"                  INT, 
     "payment_month"       DATE, 
     "monthly payment"     NUMERIC, 
     "principal payment"   NUMERIC, 
     "interest"            NUMERIC, 
     "principal remaining" NUMERIC
  ) ON COMMIT DROP; 

/*Insert first 0 row where everything except the principal remaining is 0*/
Insert into annuity values (0,current_date, 0,0,0,row_remaining_payment);

/*For every payment period calculate the interest, principal payment and remaining payment and insert into the temp table every iteration with a date that has interval offset based on current iteration.*/
FOR period_month IN 1.."period in months" LOOP
	row_interest = row_remaining_payment * "year interest rate" / 100 / 12; 
	row_principal_payment = monthly_payment - row_interest; 
	row_remaining_payment = row_remaining_payment - row_principal_payment; 
	if row_remaining_payment < 0 then
		row_remaining_payment = 0;
	end if;
	
	Insert into annuity values (period_month,current_date + interval '1 month' * period_month, monthly_payment,row_principal_payment,row_interest,row_remaining_payment);
END LOOP;

/*Returns the table with rounded numerics*/
RETURN QUERY SELECT 
annuity."nr",
to_char(annuity."payment_month",'YYYY - MM'),
Round(annuity."monthly payment"::numeric,2),
Round(annuity."principal payment"::numeric,2),
Round(annuity."interest"::numeric,2),
Round(annuity."principal remaining"::numeric,2) from annuity;

DROP TABLE annuity;

END;
$$ LANGUAGE plpgsql;

SELECT * from get_annuity_table(2555,12,5);
